import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';
import { Http} from '@angular/http';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DataService {
  private _url = './assets/mock-api-data';

  constructor(
    private _http:HttpClient
  ) { }


  getAllPatients(){
  return this._http.get(`${this._url}/patients.json`)
    .map((data:any)=>{
      return data
    })
  }

  getPatientActivity(id){
    return this._http.get(`${this._url}/patients/${id}/summary.json`)
      .map((data:any)=>{
        return data
      })

  }

  getActivityDefinitions(){
    return this._http.get(`${this._url}/definitions/activities.json`)
      .map((data:any)=>{
        return data})

  }

}
