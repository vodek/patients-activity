import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from './data.service';
import { ModalDirective } from 'ngx-bootstrap/modal/modal.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChild('moreInfoModal') moreInfoModal
  public patients: Array<any>
  public patientsFiltered: Array<any>
  private definitions: Array<any>
  private vigorousDefArr: Array<string>
  private moderateDefArr: Array<string>
  private minimumActivityTime: number = 150
  public patientInfo: any

  constructor(
    private dataService: DataService
  ) {

    this.vigorousDefArr = []
    this.moderateDefArr = []


  }

  ngOnInit() {
    this.dataService.getAllPatients()
      .subscribe((res: any) => {
        if (res) {
          //returns combined values from apis 
          this.buildPatientsArray(res)
        }
      })


    this.dataService.getActivityDefinitions()
      .subscribe((res: any) => {
        if (res) {
          res.filter((res) => {
            if (res.intensity === 'vigorous') {
              //array of vigorous activities names according to given definitions 
              this.vigorousDefArr.push(res.activity)
            }
            if (res.intensity === 'moderate') {
              //array of moderate activities names according to given definitions 
              this.moderateDefArr.push(res.activity)
            }
          })
     

        }
      })

  }

  buildPatientsArray(e) {
    e.forEach((patient) => {
      this.dataService.getPatientActivity(patient.id)
        .subscribe(res => {
          if (res) {
            //add activities array to patient
            patient.activities = res
            //set initial values for activities time
            patient.totalModerateActivityMinutes = 0
            patient.totalVigoursActivityMinutes = 0

            res.forEach(activity => {
              if (this.vigorousDefArr.indexOf(activity.activity) > -1) {
                //sum up vigorous activity time 
                patient.totalVigoursActivityMinutes += activity.minutes
              }
              if (this.moderateDefArr.indexOf(activity.activity) > -1) {
                //sum up moderate activity time
                patient.totalModerateActivityMinutes += activity.minutes
              }
            });
            //set true if activities proportions are 150 or more
            patient.levelOfActivityPassed = this.checkActivityLevel(patient.totalVigoursActivityMinutes, patient.totalModerateActivityMinutes)
          }
        })


    })
    this.patients = e
    this.patientsFiltered = e


  }
  //method to check if required activity level met 
  checkActivityLevel(v: number, m: number) {

    return ((v * 2 + m) >= this.minimumActivityTime);

  }


  //show more info about patient in modal
  showMoreInfo(id: number) {

    let i = this.patients.findIndex((a) => {
      return a.id == id
    })

    this.patientInfo = this.patients[i]



    this.moreInfoModal.show()

  }

  closeMoreInfoModal() {
    this.patientInfo = null
    this.moreInfoModal.hide()
  }

  searchPatients(e) {


    if (e) {
      this.patientsFiltered = this.patients.sort().filter((item) => {

        let transformedItem = item.name.toLowerCase();

        if (transformedItem.indexOf(e.toLowerCase()) > -1) {

          return item;
        }

      });

    } else {
      this.patientsFiltered = this.patients
    }



  }

  //sorting is happening here
  sort(e: string, direction: string) {



    switch (e) {
      case 'name':

        this.patientsFiltered.sort((a: any, b: any) => {
          let prev = a.name.toUpperCase()
          let next = b.name.toUpperCase()

          if (prev < next) {
            let val1
            direction === 'asc' ? val1 = -1 : val1 = 1
            return val1
          }
          if (prev > next) {
            let val2
            direction === 'asc' ? val2 = 1 : val2 = -1
            return val2

          }
          return 0;
        })
        break;
      case 'activity':

        this.patientsFiltered.sort((a: any, b: any) => {
          let prev = a.totalModerateActivityMinutes + a.totalVigoursActivityMinutes * 2
          let next = b.totalModerateActivityMinutes + b.totalVigoursActivityMinutes * 2

          if (prev < next) {
            let val1
            direction === 'asc' ? val1 = -1 : val1 = 1
            return val1
          }
          if (prev > next) {
            let val2
            direction === 'asc' ? val2 = 1 : val2 = -1
            return val2

          }
          return 0;
        })
        break;

        default:
        this.patientsFiltered.sort((a: any, b: any) => {
          let prev = a.id
          let next = b.id

          if (prev < next) {
           return -1
          }
          if (prev > next) {
            return 1

          }
          return 0;
        })

        break;

    }



  }






}
